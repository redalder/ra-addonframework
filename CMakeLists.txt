cmake_minimum_required(VERSION 3.13)

if(NOT REAL_INSTALL)
    set(CMAKE_INSTALL_LIBDIR "${CMAKE_CURRENT_BINARY_DIR}/install/lib" CACHE STRING "Fix non root build" FORCE)
    set(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/install" CACHE STRING "Fix non root build" FORCE)
endif()

project(ra-addonframework VERSION 0.1 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)

set(YAML_CPP_BUILD_TESTS OFF CACHE BOOL "Fix failing test compilations" FORCE)
add_subdirectory(ext/yaml-cpp)
add_subdirectory(ext/spdlog)

if(WIN32)
    find_package(ZLIB)
    if(NOT ZLIB_FOUND)
        message(STATUS "zlib not found as system library, building it")
        add_subdirectory(ext/zlib)
        set(ZLIB_SELF_BUILT TRUE)

        add_custom_target(zlib-install COMMAND cmake -P ${CMAKE_CURRENT_BINARY_DIR}/ext/zlib/cmake_install.cmake
                                                        -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX} -DCMAKE_INSTALL_LIBDIR=${CMAKE_INSTALL_LIBDIR})
        set(ZLIB_LIBRARY ${CMAKE_INSTALL_PREFIX}/lib/libzlib.dll.a)
        set(ZLIB_INCLUDE_DIR ${CMAKE_INSTALL_PREFIX}/include/zlib)
        set(ZLIB_VERSION_STRING "1.2.11")
    endif()

    add_subdirectory(ext/dokany/dokan_fuse)

    add_subdirectory(ext/dlfcn-win32)

    add_subdirectory(ext/libzip)
    add_custom_target(libzip-include-patch COMMAND git apply ${CMAKE_CURRENT_LIST_DIR}/libzip-fix-include.patch
	|| echo "[WARN] Failed to apply patch libzip-fix-include.patch, you can ignore this if it is not the first build." 
	WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR})
    add_dependencies(zip libzip-include-patch)
    if(NOT ZLIB_FOUND)
         add_dependencies(zip zlib-install)
    endif()

    add_custom_target(libzip-install COMMAND cmake -P ${CMAKE_CURRENT_BINARY_DIR}/ext/libzip/cmake_install.cmake 
                                                      -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX} -DCMAKE_INSTALL_LIBDIR=${CMAKE_INSTALL_LIBDIR})
    add_dependencies(libzip-install zip)

    add_custom_target(fuse-zip-catch-by-const-reference-patch COMMAND git apply ${CMAKE_CURRENT_LIST_DIR}/fuse-zip-fix-catch-by-const-reference.patch
        || echo "[WARN] Failed to apply fuse-zip-fix-catch-by-const-reference.patch, you can ignore this if it is not the first build."
        WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR})

    add_custom_target(fuse-zip COMMAND CC=${CMAKE_C_COMPILER} CXX=${CMAKE_CXX_COMPILER} 
				       PKG_CONFIG_PATH=$ENV{PKG_COFIG_PATH}:${CMAKE_INSTALL_LIBDIR}/pkgconfig:${CMAKE_CURRENT_LIST_DIR}/ext/dokany/dokan_fuse:${CMAKE_SOURCE_DIR}/ext/libzip make -f
				       ${CMAKE_CURRENT_LIST_DIR}/fuse-zip-makefile-windows clean lib
			       WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/ext/fuse-zip DEPENDS dokanfuse1 libzip-install)
    add_dependencies(fuse-zip fuse-zip-catch-by-const-reference-patch)
endif()

add_subdirectory(lib)
add_subdirectory(test)
