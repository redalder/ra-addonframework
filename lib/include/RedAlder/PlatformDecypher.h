//
// Created by main on 1/3/19.
//

#ifndef RA_ADDONFRAMEWORK_PLATFORMDECYPHER_H
#define RA_ADDONFRAMEWORK_PLATFORMDECYPHER_H

#include <string>

// Based on https://sourceforge.net/p/predef/wiki/Architectures/ and https://stackoverflow.com/questions/3213037/determine-if-linux-or-windows-in-c

constexpr const char* DecypherPlatform() {
#if defined(i386) || defined(__i386) || defined(__i386__) || defined(_M_I86)
    #if (defined(WIN32) || defined(_WIN32) || defined(__WIN32)) && !defined(__CYGWIN__)
    return "windows-x86";
    #endif
    #if defined(__unix__) && !defined(__linux__)
    return "unix-x86";
    #endif
    #if defined(__linux__) && defined(__unix__)
    return "linux-x86";
    #endif
#endif
#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__)|| defined(__x86_64) || defined(_M_AMD64) || defined(_M_AMD64)
    #if (defined(WIN32) || defined(_WIN32) || defined(__WIN32)) && !defined(__CYGWIN__)
    return "windows-x86_64";
    #endif
    #if defined(__unix__) && !defined(__linux__)
    return "unix-x86_64";
    #endif
    #if defined(__linux__) && defined(__unix__)
    return "linux-x86_64";
    #endif
#endif
#if (defined(__arm__) || defined(_M_ARM)) && !defined(__aarch64__)
    #if (defined(WIN32) || defined(_WIN32) || defined(__WIN32)) && !defined(__CYGWIN__)
    return "windows-arm";
    #endif
    #if defined(__unix__) && !defined(__linux__)
    return "unix-arm";
    #endif
    #if defined(__linux__) && defined(__unix__)
    return "linux-arm";
    #endif
#endif
#if (defined(__arm__) || defined(_M_ARM)) && defined(__aarch64)
    #if (defined(WIN32) || defined(_WIN32) || defined(__WIN32)) && !defined(__CYGWIN__)
    return "windows-arm64";
    #endif
    #if defined(__unix__) && !defined(__linux__)
    return "unix-arm64";
    #endif
    #if defined(__linux__) && defined(__unix__)
    return "linux-arm64";
    #endif
#endif
}

#endif //RA_ADDONFRAMEWORK_PLATFORMDECYPHER_H
