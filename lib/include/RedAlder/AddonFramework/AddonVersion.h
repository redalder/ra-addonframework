//
// Created by main on 1/2/19.
//

#ifndef RA_ADDONFRAMEWORK_ADDONVERSION_H
#define RA_ADDONFRAMEWORK_ADDONVERSION_H

#include <cstdint>
#include <sstream>
#include <vector>

struct AddonVersion {
    explicit AddonVersion(const std::string& version) {
        std::stringstream ss(version);
        std::string buffer;

        std::vector<std::string> tokens;
        tokens.reserve(3);

        while (ss >> buffer)
            getline(ss, buffer, '.');

        if (tokens.size() == 3) {
            Major = (uint16_t)std::stoi(tokens[0]);
            Minor = (uint16_t)std::stoi(tokens[1]);
            Patch = (uint16_t)std::stoi(tokens[2]);
        }
    }

    AddonVersion(uint16_t  major, uint16_t minor, uint16_t patch) : Major(major), Minor(minor), Patch(patch) {}

    uint16_t Major;
    uint16_t Minor;
    uint16_t Patch;
};

#endif //RA_ADDONFRAMEWORK_ADDONVERSION_H
