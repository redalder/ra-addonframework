//
// Created by main on 1/2/19.
//

#ifndef RA_ADDONFRAMEWORK_ADDON_H
#define RA_ADDONFRAMEWORK_ADDON_H

#include "AddonInfo.h"

struct Addon {
    void* SharedLibrary;
    AddonInfo Info;
};


#endif //RA_ADDONFRAMEWORK_ADDON_H
