//
// Created by main on 1/2/19.
//

#ifndef RA_ADDONFRAMEWORK_ADDONINFO_H
#define RA_ADDONFRAMEWORK_ADDONINFO_H

#include <string>

#include "AddonVersion.h"

struct AddonInfo {
    std::string Name;
    std::string ResourceLocation;
    std::string DynamicLibraryLocation;
    AddonVersion Version;
};


#endif //RA_ADDONFRAMEWORK_ADDONINFO_H
