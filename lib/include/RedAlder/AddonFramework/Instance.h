 //
// Created by main on 1/2/19.
//

#ifndef RA_ADDONFRAMEWORK_INSTANCE_H
#define RA_ADDONFRAMEWORK_INSTANCE_H

#include <string>
#include <map>

#if(__MINGW32__)

#include <mingw.thread.h>
#include <mingw.mutex.h>
#include <mingw.condition_variable.h>

#endif

#include <spdlog/spdlog.h>

#include "Addon.h"

class Instance {
public:
    Instance(std::string addonsDir, std::string mountsDir, std::shared_ptr<spdlog::logger> logger);
    Instance(const char* addonsDir, const char* mountsDir, std::shared_ptr<spdlog::logger> logger);
    Instance(char* addonsDir, char* mountsDir, std::shared_ptr<spdlog::logger> logger);

    ~Instance();

    std::map<std::string, Addon> LoadedAddons;
protected:
private:
    std::string _mountsDir;
    std::string _addonsDir;
    std::shared_ptr<spdlog::logger> _logger;

    void loadAddon(std::string addonDir);
    void loadLibrary(Addon& addon);
    void mountZipFile(std::string zipFile, std::string mountPoint);
    void umountZipFiles();
    void unloadLibraries();
};


#endif //RA_ADDONFRAMEWORK_INSTANCE_H
